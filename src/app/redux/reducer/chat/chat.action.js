(function() {
    'use strict';

    angular
        .module('app.redux')
        .service('ChatAction', ChatAction);

    function ChatAction(AppStore, AppSocket, $q, Immutable, $rootScope, api, $state) {

        var activeGroupName;

        var histoyUserPageNo = 0;

        var notificationHandle = null;

        return {

            getCurrentPageNo: function() {
                return histoyUserPageNo;
            },

            blockVisitor: function(groupName, flag) {
                if (flag === 0) {
                    AppSocket.unBlockClient(groupName);
                } else {
                    AppSocket.blockClient(groupName);
                }
            },

            onBlockVisitor: function(groupName, action) {
                var index = this.findIndexOfChat(groupName);

                if (index !== -1) {
                    AppStore.dispatch({
                        type: action,
                        chatIndex: index
                    });
                }

            },

            setActiveGroupName: function(name) {

                var index = this.findIndexOfChat(activeGroupName);
                var lastActiveUser = this.getState().activeUsers[index];

                if (lastActiveUser && lastActiveUser.loadedPage && lastActiveUser.loadedPage >= 1) {
                    AppStore.dispatch({
                        type: 'KEEP_10_MESSAGE',
                        userIndex: index
                    })
                }

                activeGroupName = name;


                var user = this.getState().activeUsers.filter(function(item) { return item.groupName === name; })[0]


                if (user.messages === undefined || !(user.messages.length >= 1 && user.messages.length < 10)) {
                    user.messages = [];
                    AppSocket.loadMoreChat(name, 0);
                    histoyUserPageNo = 0;
                }

                return this;
            },

            loadCurrentMacros: function(website) {

                return api.chatMacro.query({ 'name': website }).$promise;
            },


            callLoadMoreMessage: function() {

                var index = this.findIndexOfChat(activeGroupName);

                if (index !== -1) {
                    var user = this.getState().activeUsers[index];

                    if (!user.stopLoading && !angular.isUndefined(user.loadedPage)) {
                        var toLoad = user.loadedPage + 1;

                        AppSocket.loadMoreChat(activeGroupName, toLoad);
                    }
                }
            },

            loadUserHistory: function(res) {

                histoyUserPageNo = histoyUserPageNo + 1;

                AppStore.dispatch({
                    type: 'LOAD_HISTORY_USER',
                    payload: res
                });
            },

            loadMoreMessage: function(groupIndex, message) {
                var user = this.getState().activeUsers[groupIndex];

                if (user && message) {
                    AppStore.dispatch({
                        userIndex: groupIndex,
                        type: 'LOAD_MORE_USER_MESSAGE',
                        payload: message,
                        loadedPage: (!angular.isUndefined(user.loadedPage) ? (user.loadedPage + 1) : 0)
                    });

                    return;
                }

                if (message === null) {
                    AppStore.dispatch({
                        userIndex: groupIndex,
                        type: 'SET_STOP_SCROLL'
                    });
                }

            },

            loadReferrals: function() {
                api.referral.query(function(res) {
                    AppStore.dispatch({
                        'type': 'LOAD_REFERRAL',
                        'payload': res
                    });
                });
            },

            saveReferral: function(referral) {
                return api.referral.save(referral, function(res) {
                    AppStore.dispatch({
                        'type': 'SAVE_REFERRAL',
                        'payload': res
                    });
                }).$promise;
            },

            clearActiveGroupName: function() {
                activeGroupName = '';
            },

            ipInfoUpdated: function(info) {
                var chats = this.getState().activeUsers;

                var index = this.getState().activeUsers.map(function(u) { return u.groupName }).indexOf(info.GroupName);

                if (index !== -1) {
                    AppStore.dispatch({

                        index: index,
                        geoPayload: info.IpInfoDb,
                        sitePages: info.SitePage,
                        currentPage: info.CurrentPage,
                        currentUrl : info.CurrentUrl,
                        type: 'EDIT_IPINFO_DB'

                    });

                    console.log(AppStore.getState());
                }

            },

            getActiveGroupName: function() {
                return activeGroupName;
            },

            windowBlurred: function() {
                AppStore.dispatch({
                    type: 'BLURED_TAB'
                });
            },

            windowFocused: function() {
                var state = this.getState();

                if (state.unseenNotification) {
                    document.title = state.pageTitle;
                }

                AppStore.dispatch({
                    type: 'FOCUSED_TAB'
                });

                if (activeGroupName) {
                    this.clearUndreadOnChat(activeGroupName);
                }

            },

            clearUndreadOnChat: function(groupName) {

                var activeUsers = this.getState().activeUsers;

                var index = activeUsers.map(function(u) { return u.groupName }).indexOf(groupName);

                if (index !== -1 && activeUsers[index].unread) {
                    AppStore.dispatch({
                        type: 'SET_UNREAD_NULL',
                        index: index
                    });

                    AppSocket.markAsSeen(groupName);
                }
            },


            notifyUnseenNotification: function(message, data) {

                var unseen = this.getState().unseenNotification;

                unseen = isNaN(unseen) ? 0 : unseen;

                unseen = unseen + 1;

                AppStore.dispatch({
                    type: 'NOTIFCATION_UNSEEN',
                    payload: unseen,
                    notificationData: data
                });

                document.title = '*' + message + '(' + unseen + ')';
            },

            getState: function(i) {
                return i ? Immutable.fromJS(AppStore.getState().chat) : AppStore.getState().chat;
            },

            init: function() {
                var state = this.getState();

                if (!state.init) {
                    AppSocket.init(this);
                }

                AppStore.dispatch({
                    type: 'INIT'
                });

                return $q.when(true);
            },

            /*
					Set all connected clients in chat store
        		*/

            setAllConnectedClients: function(users) {

                users = users || []

                AppStore.dispatch({
                    type: 'SET_ACTIVE_USERS',
                    users: users
                });
            },

            /*******
             *    Find the group object by id
             *    
             ********/

            findUser: function(value, key) {

                key = key || 'groupName';

                var state = this.getState(true).get('activeUsers');

                if (angular.isObject(state)) {
                    return state.findIndex(function(item) { return item.get(key) === value; });
                }

                return false;
            },

            /*
                Set new user when they arrived
            */

            setNewUserArrived: function(user) {

                var index = this.findUser(user.groupName);

                if (index === -1 || index === false) {

                    AppStore.dispatch({
                        type: 'NEW_USER_ARRIVED',
                        user: user
                    });

                    return;
                }

                AppStore.dispatch({
                    type: 'USER_CAME_BACK',
                    index: index
                });
            },

            setUserOffline: function(groupName) {

                var index = this.findUser(groupName);

                if (index !== -1) {

                    AppStore.dispatch({
                        type: 'SET_USER_OFFLINE',
                        index: index
                    });
                }
            },

            /*
                Remove users from list when some one lefts
            */

            userLeft: function(user) {

                var index = this.findUser(user.id);

                if (index !== -1) {

                    AppStore.dispatch({
                        type: 'USER_LEFT',
                        index: index
                    });
                }
            },

            /***********************
             *    User New
             *    Message Arrived
             *    
             ***********************/

            showLocalPushNotification: function(message, entity) {
                if (notificationHandle !== null) {
                    notificationHandle.close();
                    notificationHandle = null;
                }

                notificationHandle = new Notification('You have new massage ' + ': ' + message);

                notificationHandle.onclick = function() {
                    if ($state.current.name == 'app.chat') {
                        $rootScope.$broadcast('open-chat', entity);
                    } else {
                        $state.go('app.chat').then(function() {
                            $rootScope.$broadcast('open-chat', entity);
                        });
                    }

                    window.focus();

                    notificationHandle.close();

                    notificationHandle = null;
                }
            },

            userMessageArived: function(message) {
                var index = this.findUser(message.groupName);

                var state = this.getState();

                if (index !== -1) {

                    var payload = { 'message': message, unread: undefined };

                    if (state.unseenNotification && message.User.IsAdmin === false) {
                        this.showLocalPushNotification(message.Message, state.activeUsers[index]);

                        payload.unread = !isNaN(state.activeUsers[index].unread) ? (state.activeUsers[index].unread + 1) : 1;
                    }

                    AppStore.dispatch({
                        type: 'NEW_MESSAGE_ARRIVED',
                        payload: payload,
                        index: index
                    });
                }

                $rootScope.$broadcast('chat:message-received', { 'message': message, 'notify': this.getState().unseenNotification });
            },

            /*
            	Accpet the user call and make it call accepted in the list
            */

            acceptCall: function(groupName) {

                AppSocket.callAccepted(groupName);

                return $q.when(this.getState().activeUsers.filter(function(item) { return item.groupName === groupName; })[0]);
            },

            broadcastCAll: function(event, data) {

                var groupName = angular.isObject(data) ? data.groupName : data;

                var activeUsers = this.getState(true).get('activeUsers');

                activeUsers.forEach(function(value, index) {
                    if (value.get('groupName') === groupName && value.has('call_accepted_user') === false) {

                        $rootScope.$broadcast(event, data);

                        return false;
                    }
                });
            },

            setActiveChatUser: function(user) {
                AppStore.dispatch({
                    type: 'SET_ACTIVE_CHAT',
                    user: user
                });
            },

            setAllGroupMessages: function(messages) {

                var activeUsers = this.getState(true).get('activeUsers');

                messages.forEach(function(message, key) {

                    activeUsers = activeUsers.update(
                        activeUsers.findIndex(function(user) {
                            return user.get('groupName') === message.groupName;
                        }),
                        function(user) {
                            return user.set('messages', message.EventMessage);
                        }
                    );
                });

                this.setAllConnectedClients(activeUsers.toJS());
            },

            loadCurrentUserMessage: function(message) {

                var index = this.getState().activeUsers.map(function(u) {

                    return u.groupName;

                }).indexOf(message.groupName);

                if (index !== -1) {
                    this.loadMoreMessage(index, message);
                }
            },

            setIsTyping: function(groupName) {
                var state = this.getState().activeUsers;

                var index = state.map(function(item) {
                    return item.groupName;
                }).indexOf(groupName.g);

                if (index !== -1) {
                    AppStore.dispatch({
                        type: 'IS_TYPING',
                        index: index,
                        payload: groupName
                    });

                    $rootScope.$broadcast('chat:scroll-bottom');
                }

            },


            findIndexOfChat: function(name) {
                var state = this.getState().activeUsers;

                var index = state.map(function(item) {
                    return item.groupName;
                }).indexOf(name);

                return index !== -1 ? index : false;
            },

            customerInfoChanged: function(info) {

                var index = this.findIndexOfChat(info.groupName);

                if (index !== -1) {
                    AppStore.dispatch({

                        userIndex: index,
                        type: 'CUSTOMER_INFO_CHANGED',
                        payload: info
                    });
                }

            },

            setStoppedTyping: function(groupName) {
                var state = this.getState().activeUsers;

                var index = state.map(function(item) {
                    return item.groupName;
                }).indexOf(groupName.g);

                if (index !== -1) {
                    AppStore.dispatch({
                        type: 'STOPPED_TYPING',
                        index: index
                    });
                }
            },

            clearAllVisitors: function() {
                AppStore.dispatch({
                    type: 'CLEAR_ACTIVE_USERS',
                })
            }
        };
    }
})();