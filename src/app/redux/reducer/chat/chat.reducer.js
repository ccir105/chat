(function() {
    'use strict';

    angular
        .module('app.redux')
        .service('ChatReducer', ChatReducer);

    function ChatReducer(Immutable) {
        return function(state, action) {
            if (!state) {
                state = {
                    init: false,
                    activeUsers: null,
                    tabFocused: true,
                    pageTitle: document.title,
                    unseenNotification: null,
                    notificationData: [],
                    referral: null
                };
            }

            switch (action.type) {

                case 'INIT':
                    state.init = true;
                    state.tabFocused = true;
                    break;

                case 'FOCUSED_TAB':
                    state.tabFocused = true;
                    state.unseenNotification = null;
                    break;

                case 'NOTIFCATION_UNSEEN':
                    state.unseenNotification = action.payload;
                    state.notificationData.push(action.notificationData);
                    console.log(state.notificationData);
                    break;

                case 'BLURED_TAB':
                    state.tabFocused = false;
                    break;

                case 'SET_ACTIVE_USERS':
                    state.activeUsers = state.activeUsers || []
                    state.activeUsers = action.users.concat(state.activeUsers);
                    break;

                case 'CLEAR_ACTIVE_USERS':
                    state.activeUsers = [];
                    break;

                case 'NEW_USER_ARRIVED':
                    state.activeUsers = state.activeUsers || []
                    state.activeUsers.unshift(action.user);
                    break;

                case 'USER_LEFT':
                    state.activeUsers.slice(action.index, 1);
                    break;

                case 'EDIT_IPINFO_DB':
                    state.activeUsers[action.index].IpInfoDb = action.geoPayload;
                    state.activeUsers[action.index].SitePages = action.sitePages;
                    state.activeUsers[ action.index ].CurrentPage = action.currentPage;
                    state.activeUsers[action.index].CurrentUrl = action.currentUrl;

                    break;

                case 'KEEP_10_MESSAGE':
                    state.activeUsers[action.userIndex].messages = state.activeUsers[action.userIndex].messages.slice(0, 10);
                    state.activeUsers[action.userIndex].stopLoading = false;
                    state.activeUsers[action.userIndex].loadedPage = 0;
                    break;

                case 'LOAD_MORE_USER_MESSAGE':
                    state.activeUsers[action.userIndex].messages = state.activeUsers[action.userIndex].messages || [];
                    state.activeUsers[action.userIndex].messages = action.payload.EventMessage.concat(state.activeUsers[action.userIndex].messages);
                    state.activeUsers[action.userIndex].loadedPage = action.loadedPage;
                    break;

                case 'SET_USER_MESSAGE_LOADING':
                    state.activeUsers[action.userIndex].loadingPage = action.payload;
                    break;

                case 'SET_STOP_SCROLL':
                    state.activeUsers[action.userIndex].stopLoading = true;
                    break;

                case 'CUSTOMER_INFO_CHANGED':
                    angular.extend(state.activeUsers[action.userIndex], action.payload);
                    break;

                case 'USER_CAME_BACK':
                    state.activeUsers[action.index].online = true;
                    break;

                case 'SET_USER_OFFLINE':

                    state.activeUsers[action.index].online = false;
                    break;

                case 'LOAD_HISTORY_USER':

                    state.activeUsers = state.activeUsers.concat(action.payload);

                    break;

                case 'LOAD_MESSAGE':

                    state.activeUsers[action.index].messages = action.messages;

                    break;

                case 'IS_TYPING':
                    state.activeUsers[action.index].isTyping = action.payload;
                    break;

                case 'STOPPED_TYPING':
                    state.activeUsers[action.index].isTyping = false;
                    break;

                case 'NEW_MESSAGE_ARRIVED':

                    state.activeUsers[action.index].messages = state.activeUsers[action.index].messages || [];

                    state.activeUsers[action.index].messages.unshift(action.payload.message);

                    state.activeUsers[action.index].LastMessage = action.payload.message.Message;

                    state.activeUsers[action.index].unread = action.payload.unread;

                    state.activeUsers[action.index].isTyping = false;

                    state.activeUsers = moveToFirst(state.activeUsers, action.index);

                    break;

                case 'LOAD_USER_MESSAGE':


                case 'SET_UNREAD_NULL':

                    state.activeUsers[action.index].unread = undefined;

                    break;

                case 'SET_UNREAD_MULTIPLE':

                    state.activeUsers = clearMultipleUnread(state.activeUsers, action.payload);
                    break;

                case 'LOAD_REFERRAL':
                    state.referral = action.payload;
                    break;

                case 'SAVE_REFERRAL':
                    state.referral = state.referral || [];
                    state.referral.push(action.payload);
                    break;

                case 'BLOCK_VISITOR':
                    state.activeUsers[action.chatIndex].blocked = true;
                    break;

                case 'UNBLOCK_VISITOR':
                    state.activeUsers[action.chatIndex].blocked = false;
                    break;

                default:
                    return state;
            }


            return state;
        };

        function moveToFirst(users, index) {
            users.unshift(users.splice(index, 1)[0])
            return users;
        }

        function clearMultipleUnread(activeUsers, payload) {

            for (var i = 0; i <= payload.length; i++) {
                if (activeUsers[payload[i]]) {
                    activeUsers[payload[i]].unread = undefined;
                }
            }

            return activeUsers;
        }
    }
})();