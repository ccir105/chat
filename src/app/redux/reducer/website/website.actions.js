var test;

(function ()
{
    'use strict';

    angular
        .module('app.redux')
        .service('WebsiteAction', WebsiteAction);

        function WebsiteAction(AppStore, $timeout, Immutable, $q, api, $rootScope){
        	return {
        		loadWebsites: function()
        		{
        			var websiteState = this.getState();
        			
                    var list = [];

                    return api.website.query( function(res) {
                        AppStore.dispatch({
                            type:'LOAD_WEBSITE',
                            websites: angular.copy(res)
                        });

                    }).$promise;                  
        		}, 

                onReportingOpened: function(data){
                    $rootScope.$broadcast('app:reporting', data);
                },

                onSwtichedToFree: function(id){

                    var websiteIndex = this.getWebsiteIndex(id);
                    
                    if( websiteIndex !== -1 )
                    {
                        AppStore.dispatch({
                            type: 'SWITCH_TO_NEXT_PLAN',
                            websiteIndex: websiteIndex,
                            payload: 'Free'
                        });

                        $rootScope.$broadcast('wesite:plan-changed', id);
                    }
                },

                removeWebsite: function(index, id){
                    return api.website.delete({ id: id }, function(){
                        AppStore.dispatch({
                            'type': 'DELETE_WEBSITE',
                            'index': index
                        })
                    }).$promise;
                },

        		addNewWebsite: function(website)
        		{
                    var deffer = $q.defer();

                    api.website.save(website, function(res){


                        AppStore.dispatch({
                            type: 'ADD_WEBSITE',
                            website: angular.extend(res, website)
                        });

                        deffer.resolve(res);
                    });

                    return deffer.promise;
        		},

                loadSetting: function(website)
                {
                    var state = this.getState(true);

                    test = state;

                    if( angular.isUndefined( website.website.macros ) ){

                        return api.macro.query({ id: website.website.id }, function( macro ){

                            var currentWebsite = state.getIn( [ 'list', website.index ] );

                            macro.forEach(function(value, key){

                                if( value.marcoType === 0 ){
                                    macro[key].outputMacro = value.textorURL;
                                }else{
                                    macro[key].pushMacro = value.textorURL;
                                }

                            });

                            currentWebsite.macros = macro;

                            state = state.setIn(['list', website.index ], currentWebsite).toJS();

                            state.settingOpened =  { 'website': currentWebsite,'index': website.index };

                            AppStore.dispatch({
                                type: 'UPDATE_STATE',
                                newState: state
                            });
                        }).$promise;
                    }
                    else
                    {
                        AppStore.dispatch({
                            type: 'LOAD_SETTING',
                            website: website
                        });

                        return $q.when(true);
                    }

                },

                closeSetting: function(){
                    AppStore.dispatch({
                        type: 'CLOSE_SETTING'
                    });
                },

                saveUserIdentity: function(identity){

                    var self = this;

                    return api.saveIdentity.save(identity, function(res){

                        var index = self.getState().list.map(function(v){return v.id}).indexOf(res.websiteId);

                        if( index !== -1 )
                        {
                            AppStore.dispatch({

                                type: 'UPDATE_WEBSITE_IDENTITY',
                                payload: res,
                                index: index
                            });
                        }

                    }).$promise;

                },

                getState: function(isImmutable){
                    return isImmutable ? Immutable.fromJS( AppStore.getState().website ) : angular.copy( AppStore.getState().website );
                },

                editWebsite: function(editedData){

                },

                saveWebsiteLogo: function(id, logo){

                    var index = this.getState().list.map(function(v){return v.id}).indexOf(id);

                    return api.website.save({id: id},logo, function(res){

                        AppStore.dispatch({
                            type:'EDIT_WEBSITE_LOGO',
                            index: index,
                            payload: logo.logo
                        });
                    }).$promise;
                },

                addNewMacro: function(marco){
                    var state = this.getState();

                    marco.textorURL = ( marco.pushMacro) ? marco.pushMacro : marco.outputMacro;

                    marco.marcoType = ( marco.pushMacro) ? 'push' : 'text';

                    marco.websiteId = state.settingOpened.website.id;

                    api.saveMacro.save(marco, function(res){
                            
                        var index = state.settingOpened.index;

                         AppStore.dispatch({
                            type: 'ADD_NEW_MACRO',
                            index: index,
                            payload: angular.extend(marco, res)
                        });

                    });
                },

                editMacro: function(macro){
                    
                    AppStore.dispatch({
                        type: 'EDIT_MACRO',
                        macro: macro
                    });
                },

                loadScript: function(website){

                    var state = this.getState();

                    var deffer = $q.defer();

                    if( angular.isUndefined( state.settingOpened.website.script )  )
                    {
                        api.websiteScript.get( { name: state.settingOpened.website.name } , function(res){

                            deffer.resolve(res.script);

                            AppStore.dispatch({
                                type: 'SCRIPT_LOADED',
                                index: state.settingOpened.index,
                                payload: res.script
                            });
                        });
                    }
                    else{
                        deffer.reject(true);
                    }

                    return deffer.promise;
                    
                },

                loadOperators: function(website){

                    var state = this.getState();


                    var deffer = $q.defer();

                    api.operators.query({id: state.settingOpened.website.id}, function(response){

                        // response = Immutable.fromJS(response).toJS();


                        // // AppStore.dispatch({
                        // //     type: 'LOAD_OPERATORS',
                        // //     index: state.settingOpened.index,
                        // //     payload: response
                        // // });

                        deffer.resolve( response )
                    });

                    return deffer.promise;

                },

                deleteRole: function(operator, role, index)
                {
                    var state = this.getState();

                    return api.deleteRole.save({websiteId: state.settingOpened.website.id, 'userEmail': operator.email,'roleName': role }, function(){

                    }).$promise;
                },

                addRole: function(operator, role)
                {
                    var state = this.getState();
                    return api.roles.save({websiteId: state.settingOpened.website.id,'userEmail': operator.email,'roleName': role }, function(){

                    }).$promise;
                },

                saveOperator: function(operator)
                {
                    var deffer = $q.defer();
                    var state = this.getState();
                    api.roles.save({websiteId: state.settingOpened.website.id,'userEmail': operator.email,'roleName': operator.role }, function(response){
                        deffer.resolve(response);
                    });

                    return deffer.promise;
                },

                removeOperator: function(op){
                 
                },

                saveEditingMacro: function()
                {
                    var defer = $q.defer();

                    var state = this.getState();

                    var index = state.settingOpened.index;

                    var macroIndex = state.editingMacro.index;

                    var macro = angular.copy( state.editingMacro.macro );

                    macro.marcoType = ( macro.outputMacro === '' ) ? 'push' : 'text';

                    macro.textorURL = macro.marcoType === 'push' ? macro.pushMacro : macro.outputMacro;

                    api.saveMacro.save({id: macro.id}, macro, function(res){

                        var currentWebsite = state.list[index];

                        currentWebsite.macros[ macroIndex ] = macro;

                        AppStore.dispatch({
                            type: 'AFTER_MACRO_EDIT',
                            index: index,
                            payload: currentWebsite
                        });

                        defer.resolve(currentWebsite);

                    });

                    return defer.promise;
                },

                removeMacro: function(id, index){

                    var websiteIndex = this.getState().settingOpened.index;

                    api.saveMacro.delete({'id': id}, function(){

                        AppStore.dispatch({
                            type: 'DELETE_MACRO',
                            index: index,
                            websiteIndex: websiteIndex
                        });
                    });
                },

                saveWidget: function(param)
                {
                     var websiteIndex = this.getState().settingOpened.index;

                    return api.widget.save(param, function(){

                        AppStore.dispatch({
                            type: 'SAVE_WIDGET',
                            websiteIndex: websiteIndex,
                            payload: param.widgetId
                        });
                        
                    }).$promise;
                },

                closeEditMacroForm: function(){
                    AppStore.dispatch({
                        type:'CLOSE_MACRO_EDIT'
                    });
                },

                reverForInvalidForm: function()
                {
                    var state = this.getState(true);

                    var settingOpenedObj = {
                        'website': state.getIn(
                            [
                                'list',
                                 state.getIn( ['settingOpened','index'] )
                            ]).toJS(),
                        'index': state.getIn( ['settingOpened','index'] )
                    };

                    AppStore.dispatch({
                        type: 'REVERT_SETTING',
                        current: settingOpenedObj
                    });
                },

                loadOption: function( websiteId ){

                    var index = this.getState().list.map(function(item){ return item.id }).indexOf(websiteId);

                    var deffer = $q.defer();

                    api.widgetOption.get({id: websiteId}, function(res){
                        AppStore.dispatch({
                            type: 'LOAD_OPTION',
                            payload: res,
                            index: index
                        });
                        deffer.resolve(res);
                    });

                    return deffer.promise;
                },

                getWebsiteIndex: function(id){
                    return this.getState().list.map(function(item){ return item.id }).indexOf(id);
                },

                editOption: function(websiteId, options){
                    var index = this.getState().list.map(function(item){ return item.id }).indexOf(websiteId);
                    
                    var deffer = $q.defer();

                    api.widgetOption.save({id: websiteId},options, function(res){
                        AppStore.dispatch({
                            type: 'LOAD_OPTION',
                            payload: res,
                            index: index
                        });
                        deffer.resolve(res);
                    });

                    return deffer.promise;
                },

                loadDashboardReport: function(data)
                {
                    $rootScope.$broadcast('socket:refresh-dashboard', data);
                },

                loadAnalyticsReport: function(data)
                {
                    $rootScope.$broadcast('socket:refresh-analytics', data);
                },
        	};
        }
})();