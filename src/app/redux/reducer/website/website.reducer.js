(function ()
{
    'use strict';

    angular
        .module('app.redux')
        .service('WebsiteReducer', WebsiteReducer);

        function WebsiteReducer(){

			return function(state, action)
			{
				state = state || {'list': null};
				switch(action.type){
					case 'UPDATE_STATE':
						return action.newState;
					case 'LOAD_WEBSITE':
						state.list = action.websites;
						return state;
					case 'ADD_WEBSITE':
						state.list.unshift(action.website);
						return state;
					case 'LOAD_SETTING':
						state.settingOpened = action.website;
						return state;
					case 'CLOSE_SETTING':
						state.settingOpened = undefined;
						return state;
					case 'SWITCH_TO_NEXT_PLAN':
						state.list[ action.websiteIndex ].websiteType = action.payload;
						return state;
					case 'UPDATE_WEBSITE_IDENTITY':
						state.list[action.index].userDescription = state.userDescription || {};
						angular.extend(state.list[action.index].userDescription, action.payload);
						return state;
					case 'DELETE_MACRO':
						state.settingOpened.website.macros.splice(action.index, 1);
						return state;

					case 'EDIT_WEBSITE_LOGO':
						state.list[action.index].logo = action.payload;
						state.settingOpened.website = state.list[action.index];
						return state;
					case 'EDIT_MACRO':
						state.editingMacro = action.macro;
						return state;
					case 'CLOSE_MACRO_EDIT':
						state.editingMacro = undefined;
						return state;
					case 'REVERT_SETTING':
						state.settingOpened = action.current;
						return state;
					case 'DELETE_WEBSITE':
						state.list.splice(action.index, 1);
						return state;
					case 'SCRIPT_LOADED':
						state.list[action.index].script = action.script;
						return state;
					case 'AFTER_MACRO_EDIT':
						state.list[action.index] = action.payload;
						return state;
					case 'ADD_NEW_MACRO':
						state.list[action.index].macros = state.list[action.index].macros || [];
						state.list[action.index].macros.push(action.payload);
						state.settingOpened.website = state.list[action.index]; 
						return state;
					case 'LOAD_OPERATORS':
						state.list[ action.index ].operators = action.payload;
						return state;
					case 'ADD_OPERATORS':
						state.list[ action.index ].operators = state.list[action.index].operators || [];
						state.list[action.index].operators.push(action.payload);
						return state;

					case 'SAVE_WIDGET':
						state.list[ action.websiteIndex ].widgetId = action.payload;
						return state;

					case 'REMOVE_OPERATORS':
						state.list[ action.websiteIndex ].operators.splice( action.opIndex, 1 );
						return state;
					case 'LOAD_OPTION':
						state.list[action.index].options = action.payload;
						return state;
					default:
						return state;
				}
			};
        }
})();