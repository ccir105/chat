describe('Global Action', function() {
	var appStore;
    var WebsiteAction;
    var backendFactory;
    var api;

    beforeEach( angular.mock.module('app.core', 'app.redux') );

    beforeEach(inject(function( AppStore, WebsiteAction, $httpBackend, _api_ ) {
        appStore = AppStore;
        WebsiteAction = WebsiteAction;
        backendFactory = $httpBackend;
        api = _api_;

        backendFactory.whenGET(api.baseUrl + 'Widgets/1/options').respond({
			'widgetVisible': false,
			'enableSSL': false,
			'widgetColor': '#sadsa',
			'textSize': 12,
			'textStyle': 'bold',
			'onlineText': 'Tsdafd sdfdsfdsf',
			'offlineText': 'asdsad sasad asd',
			'widgetText': 'sadsadsad sadsadas'
        });



        backendFactory.whenGET(api.baseUrl + 'Website').respond([
            {
                'id': 1,
                'name': 'asdsad'
            }   
        ]);

        backendFactory.whenPOST(api.baseUrl + 'Widgets/1/options').respond({
            'widgetVisible': true,
            'enableSSL': true,
            'widgetColor': '#newColor',
            'textSize': 15,
            'textStyle': 'bold',
            'onlineText': 'Online Text Changed',
            'offlineText': 'asdsad sasad asd',
            'widgetText': 'sadsadsad sadsadas'
        });
    }));

     it('Tesing option retrival and saving', inject(function(WebsiteAction){
     	expect(angular.isFunction(WebsiteAction.loadOption)).toBe(true);  

     	WebsiteAction.loadWebsites();

     	backendFactory.flush();

     	WebsiteAction.loadOption(1);

     	backendFactory.flush();

     	var state = appStore.getState();

     	expect(state.website.list[0].options).toBeDefined();

        expect(angular.isFunction(WebsiteAction.editOption)).toBe(true);
        
        WebsiteAction.editOption(1,{
            widgetVisible: true,
            enableSSL: true
        });

        backendFactory.flush();

        state = appStore.getState();

        expect(state.website.list[0].options.enableSSL).toEqual(true);

     }));
    
});