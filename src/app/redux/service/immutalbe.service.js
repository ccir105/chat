(function ()
{
    'use strict';

    angular
        .module('app.redux')
        .service('Immutable', service);

        function service() {
			return Immutable;
		}
})();