(function ()
{
    'use strict';

    angular
        .module('app.redux')
        .service('Redux', service);

        function service() {
			return Redux;
		}
})();