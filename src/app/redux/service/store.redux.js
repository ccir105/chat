(function ()
{
    'use strict';

    angular
        .module('app.redux')
        .service('AppStore', AppStore);

        function AppStore(Redux, WebsiteReducer, Immutable,ChatReducer)
        {
            var comb = Redux.combineReducers( { 'chat': ChatReducer, 'website': WebsiteReducer } );

        	return Redux.createStore( comb );
        }
})();