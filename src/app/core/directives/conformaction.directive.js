(function ()
{
    'use strict';

    angular
        .module('app.core')
        .directive('confirmAction', Directive);

        function Directive($parse, $mdDialog, $q){
        	return {
        		scope: { confirmAction: '&', confirmCallback: '&' },
        		link: function( scope, elem, attr ){
        			var title = attr.confirmTitle || 'Are you sure you want to delete ?';
        			var description = attr.confirmDescription || 'You cannot undo this action, so check twice';

        			elem.bind('click', function(ev){
						
						var confirm = $mdDialog.confirm()
		                  .title(title)
		                  .textContent(description)
		                  .ariaLabel('delete')
		                  .targetEvent(ev)
		                  .ok('Please do it!')
		                  .cancel('Cancel');

			            $mdDialog.show( confirm ).then(function() {
			            	var prm = scope.confirmAction();
                            if( prm && angular.isFunction( prm.then ) )
                            {
                                $q.when(prm).then(function(){

                                    scope.confirmCallback();

                                });
                            }
			            });
        			});
        		}
        	}
        }
})();