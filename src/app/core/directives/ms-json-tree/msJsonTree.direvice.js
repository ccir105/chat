(function ()
{
    'use strict';

    angular
        .module('app.core')
        .directive('msJsonTree', msJsonTree);

        function msJsonTree(){
			 return {
			 	'scope': {
			 		'msJsonTree': '='
			 	},

			 	link: function(scope, elem){
			 		elem.html( JSONTree.create(scope.msJsonTree) );
			 	}
			 }        	
        }
})();