(function() {
    'use strict';

    angular
        .module('app.core')
        .directive('bindScript', bindScript);

    function bindScript() {
        return {
            restrict: 'A',
            scope: {
                'bindScript': '='
            },
            link: function(scope, element, attrs) {
                element.html(scope.bindScript);
            }
        }
    }
})();
