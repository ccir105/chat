(function() {
    'use strict';

    angular
        .module('app.core')
        .directive('dropzone', directive);

    function directive($timeout) {
        return {
            restrict: 'A',
            scope: {
                meme: '@',
                dropzone: '=',
                resultImg: '='
            },

            template:'<div ng-if="files.length === 0" class="upload-panel"><input type="file" name="dropzone_file" id="profile-pic-file-upload" style="display:none">\
                        <h1>Select Or Drag Your Image</h1>\
                    </div>\
                    <div ng-if="files.length > 0" class="crop-zone"><img-crop image="files[0]" on-change="refreshResult($dataURI)" result-image="resultImg"></img-crop> <md-button class="md-icon-button clear-image" ng-click="removeAllImage($event)"><md-icon md-font-icon="icon-close-circle"></md-icon></md-button></div>',
            
            link: function(scope, elem) {

                scope.resultImg = scope.resultImg || '';
 
                var option = {
                    'meme': 'image'
                };

                scope.myCroppedImage = '';

                scope.files = [];

                elem.bind('click', function(e){
                     var inputElem = angular.element(elem).find('input')[0];
                     
                     if(inputElem){
                        inputElem.click();
                     }

                     e.stopPropagation();
                });

                scope.refreshResult = function($dataURI){
                    scope.resultImg = $dataURI;
                }

                $(document).on('change', '#profile-pic-file-upload', function(e){
                    validateFiles( e.target.files[ 0 ] );
                });

                elem.bind('dragover', function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                });

                elem.bind('dragenter', function(e) {
                    e.stopPropagation();

                    e.preventDefault();
                    scope.$apply(function() {
                        angular.element(elem).find('.upload-panel').addClass('on-drag-enter');
                    });
                });

                elem.bind('dragleave', function(e) {

                    e.stopPropagation();
                    e.preventDefault();
                    scope.$apply(function() {
                        angular.element(elem).find('.upload-panel').removeClass('on-drag-enter');
                    });
                });

                elem.bind('drop', function(evt) {

                    evt.stopPropagation();

                    evt.preventDefault();

                    var files = evt.originalEvent.dataTransfer.files;

                    angular.element(elem).find('.upload-panel').removeClass('on-drag-enter');

                    var f = files[0];

                    var reader = new FileReader();
                    
                    reader.readAsArrayBuffer(f);

                    reader.onload = ( function(theFile) {
                    
                        return function(e) {
                            
                            validateFiles( theFile );
                            
                        };
                    })(f);
                });

                scope.removeAllImage = function($event){
                    scope.files = [];
                    $event.stopPropagation();
                };

                function validateFiles( file ) {

                    scope.files = [];

                    var typeRegex = new RegExp( '(?:' + option.meme.split(',').join('|') + ')');
                            
                    if( !typeRegex.test(file.type ) )
                    {
                        return false;
                    }

                    var reader = new FileReader();

                    reader.onload = function(evt){
                        scope.$apply(function(){
                            scope.files.push(evt.target.result);
                        });
                    };

                    reader.readAsDataURL( file );

                }
            }
        };
    }
})();
