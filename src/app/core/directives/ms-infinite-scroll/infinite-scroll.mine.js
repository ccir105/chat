(function ()
{
    'use strict';

    angular
        .module('app.core')
            .directive('appInfiniteScroll', function ($compile, $timeout) {
              return {

                restrict: 'A',

                replace: false, 

                //this setting is important, see explanation below

                compile: function (element) {
                  element.attr('infinite-scroll', 'loadMore()');
                  element.attr('infinite-scroll-disabled', 'stopScroll');
                  element.attr('infinite-scroll-distance', '0');
                  element.attr('infinite-scroll-parent', 'true');
                  element.attr('infinite-scroll-use-document-bottom','false');
                  element.attr('resource-type', element.attr('app-infinite-scroll'));
                  element.removeAttr('app-infinite-scroll');

                  return function (scope, ele, attr, ctrl) {

                    scope.loadMore = function () {

                        scope.stopScroll = true;

                        // scope.loadMore()().then( function(){
                        //   scope.stopScroll = false;
                        // });
                    };

                    var compileFn = $compile(ele)(scope);
                  };


                }
              };
          });
})();