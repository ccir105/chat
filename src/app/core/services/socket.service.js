(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('AppSocket', service);

    function service($q, $rootScope, DataCache, Storage, api,WebsiteAction) {
        var hub;

        var action = null;

        var socket = {};

        function handleSocketBroadcast(res, callBack, part, message, $q) {
            var focused = action.getState().tabFocused;

            if (message && focused === false) {

                action.notifyUnseenNotification(message, res);
            }

            debug(part, res);
            
            callBack(res);
        }

        function debug(part, res)
        {
            if( localStorage.verbose == "1")
            {
                console.info('[INFO] ', part, res);
            }

            if( localStorage.verbose == "2" )
            {
                alert(part + ' ' + JSON.stringify(res));
            }
        }

        socket.init = function(act) {
            action = act;

            $.connection.hub.url = api.domainUrl + 'signalr';

            hub = $.connection.customerSupportHub;

            $.connection.hub.qs = 'isAdmin=true';

            socket.connect();

            $.connection.hub.disconnected(function() {
               socket.setInfoForUser(false);
               
               setTimeout(function()
               {
                   socket.connect();
               
               }, 5000);
            });
         
            // $.connection.hub.reconnecting(function () {
                

            //     console.log('Reconnecing..');

            //     act.clearAllVisitors();

            //     setInterval(socket.connect, 5000);

            // });

            // $.connection.hub.reconnected(function() {
            //     console.log('Reconnected..');
            //     socket.setInfoForUser(true);
            // });
        };

        socket.setInfoForUser = function(flag){
            setTimeout(function(){
                $rootScope.$apply(function(){
                    $rootScope.singlarConnected = flag;
                })
            })
        }

        socket.connect = function() {
            
            if(hub && hub.client)
            {
                socket.registerChatListener(hub);
            }


            $.connection.hub.start(function(){

                socket.setInfoForUser(true);

                hub && hub.server.adminAuthorization('masterstill@gmail.com', 'test123');

                $rootScope.$broadcast('singlar:connected');
            })
        }

        socket.registerChatListener = function(hub) {

            hub.client.onJoiningAllClients = function(users) {

                handleSocketBroadcast(users, function() {

                    action.setAllConnectedClients(users);

                }, 'onJoiningAllClients');
            };


            hub.client.onSendPreviousMessage = function(messageList) {

                action.loadCurrentUserMessage(messageList);
            };

            hub.client.onNewUserAuthorization = function(user) {
                handleSocketBroadcast(user, function() {
                    action.setNewUserArrived(user);
                }, 'onNewUserAuthorization');
            };

            hub.client.onUserOffline = function(user) {
                handleSocketBroadcast(user, function() {
                    action.setUserOffline(user);
                }, 'onUserOffline');
            };

            hub.client.onNewMessage = function(message) {
                handleSocketBroadcast(message, function() {
                    action.userMessageArived(message);

                }, 'onNewMessage', message.User.fullName);
            };

            hub.client.onCallingForSupport = function(groupName) {

                handleSocketBroadcast(groupName, function() {
                    action.broadcastCAll('support:call_arrived', groupName);
                }, 'onNewMessage');
            };

            hub.client.PreviousMessages = function(connectionId) {
                handleSocketBroadcast(connectionId, function() {}, 'PreviousMessages');
            };

            hub.client.onSupportRequestAccepted = function(supportUser) {
                handleSocketBroadcast(supportUser, function() {
                    action.broadcastCAll('support:request-accepted', supportUser);
                }, 'onSupportRequestAccepted');
            };

            hub.client.onNewToastMessageReceived = function(message) {
                $rootScope.$broadcast('socket:new-toast', message);
            }

            hub.client.onUserTyping = function(response) {
                handleSocketBroadcast(response, function() {
                    action.setIsTyping(response);
                });
            };

            hub.client.onUserStoppedTyping = function(response) {
                handleSocketBroadcast(response, function() {
                    action.setStoppedTyping(response);
                });
            };

            hub.client.onMessageSeen = function(groupName) {
                action.clearUndreadOnChat(groupName);
            }

            hub.client.onGeoInformation = function(info) {
                action.ipInfoUpdated(info);
            }

            hub.client.onCustomerInfoChanged = function(customerInfo) {
                action.customerInfoChanged(customerInfo);
            }

            hub.client.onLoadMoreClients = function(list) {
                action.loadUserHistory(list);
            }

            hub.client.sendMessageToAllClients = function(data){
                $rootScope.$broadcast('message:sent-all', data);
            }

            hub.client.onAdvertChanged = function(advert){
                $rootScope.$broadcast('on:advert-changed', advert);
            }

            hub.client.onUnBlocked = function(groupName){
                action.onBlockVisitor(groupName, 'UNBLOCK_VISITOR');
            }

            hub.client.onBlocked = function(groupName){
                action.onBlockVisitor(groupName, 'BLOCK_VISITOR');
            }

            hub.client.onUpdateMe = function(str){
                hub.server.updateMe(str);
            }

            hub.client.onSwitchedToFreePlan = function(id){
                WebsiteAction.onSwtichedToFree(id);
            }

            hub.client.onSendCoreReport = function(data)
            {
                WebsiteAction.onReportingOpened(data);
            }

            hub.client.onDashBoardReport = function(data){
                WebsiteAction.loadDashboardReport(data);
            }

            hub.client.onAnalyticsReport = function(data){
                WebsiteAction.loadAnalyticsReport(data);
            }

        }

        socket.sendMessage = function(message, groupName) {
            hub.server.message(message, groupName);
        };

        socket.unBlockClient = function(groupName){
            hub.server.unBlock(groupName);
        }

        socket.blockClient = function(groupName){
            hub.server.block(groupName);
        }

        socket.callAccepted = function(groupName) {

            hub.server.supportRequestAccepted(groupName, $rootScope.me.fullName);

        };

        socket.userTyping = function(groupName) {
            hub.server.userTyping(groupName);
        };

        socket.stoppedTyping = function(groupName) {
            hub.server.userStoppedTyping(groupName);
        };

        socket.markAsSeen = function(groupName) {
            hub.server.messageSeen(groupName);
        };

        socket.loadIpInfoDb = function(groupName) {
            hub.server.geoInformation(groupName);
        };

        socket.loadMoreChat = function(groupName, page) {

            hub.server.sendPreviousMessage(groupName, page);

        };

        socket.loadMoreClients = function(page) {

            hub.server.loadMoreClients(page);

        };

        socket.sendMessageToAll = function(message, id) {
            hub.server.sendMessageToAllClients(message, id);
        }

        socket.swtichToFreePlan = function(id)
        {
            hub.server.switchedToFreePlan(id);
        }

        socket.openReporting = function(param)
        {
            hub.server.sendCoreReport(param);
        }

        socket.getDashboardData = function(id){
            hub.server.dashBoardReport(id);
        }

        socket.askForClientInfo = function(groupName)
        {
            hub.server.getClientInformation(groupName);
        }

        socket.getAnalyticsReport = function(websiteId){
            hub.server.analyticsReport(websiteId);
        }

        return socket;

    }
})();
