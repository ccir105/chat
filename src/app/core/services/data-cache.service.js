(function ()
{
    'use strict';

    angular
        .module('app.core')
        .factory('DataCache', DataCache);

        function DataCache($q){
        	var cache = {
        		data: {},
        		set: setData,
        		unset: unsetData,
        		get: getData,
        		clear: clear,
        		clearAll: clearAll,
                guestData: {}
        	};


        	function setData(key, value)
        	{
        		cache.data[key] = value;
         	}

        	function unsetData(key)
        	{
        		delete(cache.data[key]);
        	}

        	function getData(key)
        	{
        		return cache.data[key];
        	}

        	function clear(keys)
        	{
        		for(var i = 0; i < keys.length ;i ++)
        		{
        			unsetData(keys[i]);
        		}
        	}

        	function clearAll()
        	{
        		cache.data = {};
        	}

        	function returnData()
        	{
        		return cache.data;
        	}

        	return cache;
        }
})();