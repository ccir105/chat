(function ()
{
    'use strict';

    angular
        .module('app.core')
        .service('Storage', LocalStorageService);

        function LocalStorageService($q)
        {
			var storageEngine = localStorage;

			return {
				get : function( key, callBack){
					var deffer = $q.defer();
					
					var value = storageEngine.getItem(key);
					
					if(value)
					{
						value = this.converInJson(value);

						if( angular.isFunction( callBack ) )
						{
							callBack(value)
						}

						deffer.resolve(value);
					}
					else
					{
						deffer.reject(false);
					}

					return deffer.promise;
				},

				converInJson: function(str)
				{
					try 
					{
				        return angular.fromJson(str);
				    }
				    catch (e) 
				    {
				        return str;
				    }
				},
				
				set: function(key, value)
				{
					var deffer = $q.defer();
					
					if(typeof value == 'object')
					{
						value = JSON.stringify(value);
					}
					
					storageEngine.setItem(key, value);
					
					if(storageEngine.hasOwnProperty(key))
					{
						deffer.resolve(true);
					}
					
					else
					{
						deffer.reject(false);
					}
					
					return deffer.promise;
				},
				
				remove: function(key)
				{
					var deffer = $q.defer();
					if(angular.isArray(key))
					{
						this.removeMultiple(key);
						deffer.resolve(true)
						return deffer.promise;
					}

				
					if(storageEngine.hasOwnProperty(key))
					{
						storageEngine.removeItem(key);

						deffer.resolve(true);
					}
					
					else
					{
						deffer.reject(false);
					}
					return deffer.promise;
				},

				removeMultiple: function(keys)
				{
					for(var i=0;i<keys.length;i++)
					{
						this.remove(keys[i]);
					}
				},

				clear: function()
				{
					storageEngine.clear();
				}
			}
        }
})();