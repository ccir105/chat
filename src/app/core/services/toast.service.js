(function()
{
	'use strict';
	angular.module('app.core').factory('Toast',function($mdToast){
		return {
			show: function(content){
				if (!content) {
					return false;
				}

				return $mdToast.show(
					$mdToast.simple()
					.content(content)
					.position(this.config.position)
					.action(this.config.action)
					.hideDelay(this.config.delay)
				);
			},

			config : {
				position:'top right',
				delay:6000,
				action:'Ok',
			},

			error: function (content) {
				if (!content) {
					return false;
				}

				return $mdToast.show(
					$mdToast.simple()
					.content(content)
					.position(this.config.position)
					.theme('error-toast')
					.action(this.config.action)
					.hideDelay(this.config.delay)
				);
			}
		}
	})
})();