(function ()
{
    'use strict';

    angular
        .module('app.core',
            [
                'ngAnimate',
                'ngAria',
                'ngCookies',
                'ngMessages',
                'ngResource',
                'ngSanitize',
                'ngMaterial',
                'pascalprecht.translate',
                'ui.router',
                'xeditable',
                'material.components.expansionPanels',
                'ngImgCrop',
                'colorpicker.module',
                'uiGmapgoogle-maps'
            ]);
})();