(function ()
{
    'use strict';

    angular
        .module('app.toolbar')
        .directive('contactScript', function ($rootScope) {
          return {
            restrict: 'A',
            link: function (scope, element, attrs) {
              element.html($rootScope.me.widgetScript);
            }
          }
        })
        .controller('ToolbarController', ToolbarController);

    /** @ngInject */
    function ToolbarController($rootScope,$scope, $mdDialog, $q, $state, $timeout,api,$sce, $mdSidenav, $translate, $mdToast, msNavigationService)
    {
        var vm = this;

        // Data
        $rootScope.global = {
            search: ''
        };

        var unsubscribeAdvertChanged = $rootScope.$on('on:advert-changed', function(ev, advert){

            if( advert && $rootScope.me)
            {
                $rootScope.me.advert = advert;
            }
        });

        var unsubscribeMeOpen = $rootScope.$on('open:me-update', function(ev){

            vm.updateInfo(null);

        });

        $scope.$on('$destroy', function(ev){
            unsubscribeAdvertChanged();
        });

        vm.output = undefined;

        vm.triggerAvatarChange = function(){
            $timeout(function(){
                $('#file-upload-drop').trigger('click');

                $('#update-info-dailog md-dialog-content').animate({'scrollTop': $('#update-info-dailog md-dialog-content')[0].scrollHeight}, 300)
            })
        }

        vm.updateInfo = function(ev){

            vm.current = angular.copy($rootScope.me);

            $mdDialog.show({
                templateUrl: 'app/main/dialogs/update-info-dailog.html',
                parent: angular.element(document.body),
                controllerAs: 'vm',
                controller: function() {
                    return vm;
                },
                clickOutsideToClose: true,
                targetEvent: ev,
            })
        };

        vm.closeDialog = function(){
            $mdDialog.hide();
        };

        vm.bodyEl = angular.element('body');
        vm.userStatusOptions = [
            {
                'title': 'Online',
                'icon' : 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon' : 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon' : 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];
        vm.languages = {
            en: {
                'title'      : 'English',
                'translation': 'TOOLBAR.ENGLISH',
                'code'       : 'en',
                'flag'       : 'us'
            },
            es: {
                'title'      : 'Spanish',
                'translation': 'TOOLBAR.SPANISH',
                'code'       : 'es',
                'flag'       : 'es'
            },
            tr: {
                'title'      : 'Turkish',
                'translation': 'TOOLBAR.TURKISH',
                'code'       : 'tr',
                'flag'       : 'tr'
            }
        };

        // Methods
        vm.toggleSidenav = toggleSidenav;
        
        vm.logout = logout;
        
        vm.goToManage = goToManage;
        
        vm.changeLanguage = changeLanguage;
        vm.setUserStatus = setUserStatus;
        vm.toggleHorizontalMobileMenu = toggleHorizontalMobileMenu;
        vm.toggleMsNavigationFolded = toggleMsNavigationFolded;
        vm.search = search;
        vm.searchResultClick = searchResultClick;
        vm.saveDetails = saveDetails;


        //////////

        init();

        /**
         * Initialize
         */
        function init()
        {
            // Select the first status as a default
            vm.userStatus = vm.userStatusOptions[0];

            // Get the selected language directly from angular-translate module setting
            vm.selectedLanguage = vm.languages[$translate.preferredLanguage()];
        }

        function saveDetails()
        {
            var param = [
                { 
                    'what': 'firstName',
                    'value': vm.current.firstName
                },

                {
                    what: 'lastName',
                    'value': vm.current.lastName
                },
                {
                    'what' : 'description',
                    'value' : vm.current.description
                }
            ];

            if( vm.resultImg.trim() !== "" )
            {
                param.push({
                    'what': 'avatar',
                    'value': vm.resultImg
                })
            }

            api.profile.save( param, function(me){
                $rootScope.me = me;
                $rootScope.$broadcast('me:loaded', me);
                $mdDialog.cancel();
            });
        }

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }

        /**
         * Sets User Status
         * @param status
         */
        
        function setUserStatus(status)
        {
            vm.userStatus = status;
        }

        /**
         * Logout Function
         */

        function logout()
        {
            location.href = $rootScope.me.logoffUrl;
        }

        function goToManage(ev)
        {

            vm.currentUser = $rootScope.me;

            vm.currentUser.manageURL = angular.isObject(vm.currentUser.manageURL) ? vm.currentUser.manageURL : $sce.trustAsResourceUrl(vm.currentUser.manageURL);

            $mdDialog.show({
                templateUrl: 'app/main/dialogs/update-iframe.dailog.html',
                parent: angular.element(document.body),
                controllerAs: 'vm',
                controller: function() {
                    return vm;
                },
                clickOutsideToClose: true,
                targetEvent: ev,
            })
        }

        /**
         * Change Language
         */
        function changeLanguage( lang )
        {
            vm.selectedLanguage = lang;

            /**
             * Show temporary message if user selects a language other than English
             *
             * angular-translate module will try to load language specific json files
             * as soon as you change the language. And because we don't have them, there
             * will be a lot of errors in the page potentially breaking couple functions
             * of the template.
             *
             * To prevent that from happening, we added a simple "return;" statement at the
             * end of this if block. If you have all the translation files, remove this if
             * block and the translations should work without any problems.
             */
            if ( lang.code !== 'en' )
            {
                var message = 'Fuse supports translations through angular-translate module, but currently we do not have any translations other than English language. If you want to help us, send us a message through ThemeForest profile page.';

                $mdToast.show({
                    template : '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + message + '</div></md-toast>',
                    hideDelay: 7000,
                    position : 'top right',
                    parent   : '#content'
                });

                return;
            }

            // Change the language
            $translate.use(lang.code);
        }

        /**
         * Toggle horizontal mobile menu
         */
        function toggleHorizontalMobileMenu()
        {
            vm.bodyEl.toggleClass('ms-navigation-horizontal-mobile-menu-active');
        }

        /**
         * Toggle msNavigation folded
         */
        function toggleMsNavigationFolded()
        {
            msNavigationService.toggleFolded();
        }

        /**
         * Search action
         *
         * @param query
         * @returns {Promise}
         */
        function search(query)
        {
            var navigation = [],
                flatNavigation = msNavigationService.getFlatNavigation(),
                deferred = $q.defer();

            // Iterate through the navigation array and
            // make sure it doesn't have any groups or
            // none ui-sref items
            for ( var x = 0; x < flatNavigation.length; x++ )
            {
                if ( flatNavigation[x].uisref )
                {
                    navigation.push(flatNavigation[x]);
                }
            }

            // If there is a query, filter the navigation;
            // otherwise we will return the entire navigation
            // list. Not exactly a good thing to do but it's
            // for demo purposes.
            if ( query )
            {
                navigation = navigation.filter(function (item)
                {
                    if ( angular.lowercase(item.title).search(angular.lowercase(query)) > -1 )
                    {
                        return true;
                    }
                });
            }

            // Fake service delay
            $timeout(function ()
            {
                deferred.resolve(navigation);
            }, 1000);

            return deferred.promise;
        }

        /**
         * Search result click action
         *
         * @param item
         */
        function searchResultClick(item)
        {
            // If item has a link
            if ( item.uisref )
            {
                // If there are state params,
                // use them...
                if ( item.stateParams )
                {
                    $state.go(item.state, item.stateParams);
                }
                else
                {
                    $state.go(item.state);
                }
            }
        }
    }

})();