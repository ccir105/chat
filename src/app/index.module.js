(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [

            // Core
            'app.core',

            // Navigation
            'app.navigation',

            // Toolbar
            'app.toolbar',
            // Sample
            'app.chat',

            'app.website',      

            'app.redux',

            'app.referral',

            'app.report',

            'app.dashboards'
        ]);
})();