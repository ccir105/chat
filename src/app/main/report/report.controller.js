(function ()
{
    'use strict';

    angular
        .module('app.report')
        .controller('ReportController', ctrl );

        function ctrl($scope, AppStore,  ChatAction, $mdDialog, $timeout, AppSocket )
        {
        	var vm = this;

            vm.callReporing = function()
            {
            	AppSocket.openReporting(vm.reportingText);
            }

            vm.reporting = []

            var unsubscribeReporting = $scope.$on('app:reporting', function(ev, data)
            {
                callSafeDigest($scope, function(){
                    vm.reporting.push(data);
                })
            });

            vm.clearReports = function(){
                vm.reporting = [];
            }

        }

        function callSafeDigest(scope, callBack) {
            setTimeout(function () {
              scope.$apply(callBack);
            });
        }
})();