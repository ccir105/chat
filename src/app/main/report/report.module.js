(function ()
{
    'use strict';

    angular
        .module('app.report',[])
        .config(config)
        .run(onRun)

      /** @ngInject */
	    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {

	        // State
	        $stateProvider.state('app.report', {
	            url: '/reports',
	            views: {
	                'content@app': {
	                    templateUrl: 'app/main/report/report.html',
	                    controller: 'ReportController as vm'
	                }
	            }
	        });
	    }

	    function onRun($rootScope, msNavigationService){

	    	function init()
	    	{
		    	msNavigationService.saveItem('apps.report', {
		            title: 'Reports',
		            icon: 'icon-cog',
		            state: 'app.report',
		            weight: 5,
		            hidden: function (){
		            	if( $rootScope.me )
		            	{
			            	return $rootScope.me.fullName !== 'Subin Adhikari'
		            	}

		            	return true;
		            }
		        });
	    	}

	    	$rootScope.$on('open:me-update', init());

	    }
})();