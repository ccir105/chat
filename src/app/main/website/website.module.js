(function ()
{
    'use strict';

    angular
        .module('app.website',[])
        .config(config);

        function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
        {
        	$stateProvider.state('app.website',{
        		url: '/website',
        		views: {
        			'content@app': {
                    	templateUrl: 'app/main/website/website.html',
		                controller: 'WebsiteController as vm'
                	}
        		} 
        	})

    	 	msNavigationServiceProvider.saveItem('apps.website', {
	            title: 'Website',
	            icon: 'icon-web',
	            state: 'app.website',
	        });
        }
})();