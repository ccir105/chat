(function() {
    'use strict';

    angular
        .module('app.website')
        .controller('WebsiteController', Controller);

    function Controller($scope, $mdSidenav,AppSocket, $mdDialog, $mdExpansionPanel, AppStore, WebsiteAction, $q, api, Toast, $rootScope) {

        var vm = this;

        vm.baseUrl = api.domainUrl;

        vm.forms = {};

        vm.plans = api.pricing.query();

        vm.addWebsite = function(ev)
        {
            vm.new = { 'name': '', alias: '' ,'operators':[]};
            
            $mdDialog.show({
                templateUrl: 'app/main/website/dialogs/website-save.dialog.html',
                parent: angular.element(document.body),
                controllerAs: 'vm',
                controller: function() {
                    return vm;
                },
                clickOutsideToClose: true,
                targetEvent: ev,
            });
        };

        vm.triggerAvatarChange = function(){
            setTimeout(function(){
                $('#file-upload-drop').trigger('click');

                $('#update-info-dailog md-dialog-content').animate({'scrollTop': $('#update-info-dailog md-dialog-content')[0].scrollHeight}, 300)
            })
        }

        vm.addOfflineTxtField = function(lastField){
            if( $.trim(lastField) !== "" )
            {
                vm.offlineTextOption.push("");
            }
        }

        vm.selectWidget = function(widget)
        {
            if( vm.websites.settingOpened.website.widget_id !== widget.id )
            {
                WebsiteAction.saveWidget({'widgetId': widget.id, 'websiteId': vm.websites.settingOpened.website.id }).then(function(){
                    vm.websites.settingOpened.website.widget_id = widget.id;
                    Toast.show('The widget is successfully changed.');
                });
            }
        }

        vm.loadWidgets = function(){
            vm.widgets = api.widget.query();
        }

        vm.addWebsiteIdentity = function(ev, website){
            
            vm.current = website.userDescription || {};

            vm.resultImg = undefined;

            vm.current = angular.copy(website.userDescription);

            vm.current.websiteId =  website.id;

            vm.dailogTitle = "You can add your own identity which will be exposed to the visitors of this site.";

            $mdDialog.show({
                templateUrl: 'app/main/dialogs/update-info-dailog.html',
                parent: angular.element(document.body),
                controllerAs: 'vm',
                controller: function() {
                    return vm;
                },
                clickOutsideToClose: true,
                targetEvent: ev,
            })
        }

        //save indentity of user on website
        vm.saveDetails = function()
        {
            vm.current.avatar = vm.resultImg || vm.current.avatar;

            WebsiteAction.saveUserIdentity(vm.current).then(function(res){

                vm.closeDialog();

                Toast.show('Your identity is updated successfully');
            });
        }
        
        //redux implementation

        var unsubscribe = AppStore.subscribe(function()
        {

            callSafeDigest($scope, function(){

                 vm.websites = AppStore.getState().website;

                if(angular.element(document).find('md-dialog').length === 0)
                {
                    if( !$rootScope || !$rootScope.me.firstName )
                    {
                        $rootScope.$broadcast('open:me-update');
                        
                        return;
                    }

                    if( vm.websites.list && vm.websites.list.length === 0 )
                    {
                        vm.websiteDialogTitle = "Lets choose a new website to add this awesome feature";
                        vm.addWebsite(null);
                    }                
                }
            })
        });

        vm.copyToClipboard = copyToClipboard;


        var unsubscribeMe = $rootScope.$on('me:loaded', function(ev, me){
            vm.me = me;
        });

        var unsubscribeMessageSuccess = $rootScope.$on('message:sent-all', function(ev, success){

            if( success )
            {
                $mdDialog.cancel();
                Toast.show('Your message has been successfully sent');
            }
        });


        var unsubscribePlanChanged = $rootScope.$on('wesite:plan-changed', function(ev, id){
            if(  vm.websites.settingOpened.website.id === id )
            {   
                vm.onPricingOpened(vm.websites.settingOpened.website.websiteType);
            }
        });
        

        $scope.$on('$destroy', function() {
            
            unsubscribe();

            unsubscribeMe();

            unsubscribeMessageSuccess();

            ensureAllFormSaved();
        });

        vm.me = $rootScope.me;

        vm.deleteWebsite = function(){

            var index = vm.websites.list.map(function(w){ return w.id }).indexOf( vm.websites.settingOpened.website.id );

            return WebsiteAction.removeWebsite(index, vm.websites.settingOpened.website.id);
        }

        vm.newOperator = {};

        WebsiteAction.loadWebsites().then(function(){
            vm.websiteLoaded = true;
        });


        vm.operators = [];

        vm.roleQuerySearch = function(query){

            query = query.toLowerCase();

            return vm.roles.filter(function(value){
                return value.toLowerCase().indexOf(query) >= 0;
            });
        }

        vm.addNewOperator = function( form )
        {
            if(form.$valid)
            {

                WebsiteAction.saveOperator(vm.newOperator).then(function(operator){
                    vm.loadOperators();
                    vm.newOperator = {};
                    form.$setPristine();
                });
            }
        }

        vm.changeOptions = function()
        {
            vm.saveForm();
        }

        vm.saveForm = function()
        {
            vm.websites.settingOpened.website.options.adminOfflineText = vm.offlineTextOption;
            //work for that situation
            WebsiteAction.editOption(vm.websites.settingOpened.website.id,angular.copy( vm.websites.settingOpened.website.options ) ).then(function(opt){
                vm.websites.settingOpened.website.options = opt;
            });
        }



        vm.loadOption = function()
        {
            WebsiteAction.loadOption(vm.websites.settingOpened.website.id).then(function(opt){

                vm.websites.settingOpened.website.options = opt || {};

                vm.websites.settingOpened.website.options.adminOfflineText = vm.websites.settingOpened.website.options.adminOfflineText || ['Thank you for contacting us. We will soon catch up']

                vm.offlineTextOption = angular.copy(vm.websites.settingOpened.website.options.adminOfflineText);

            });
        }

        function reloadOperator(){
             vm._currentWebsiteOperators = angular.copy(vm.currentWebsiteOperators);
        }

        vm.roleAdd = function(op, index, $chip){
            WebsiteAction.addRole( op, $chip, index ).then(angular.noop, reloadOperator);
        }

        vm.roleDelete = function(op, index, $chip)
        {
            WebsiteAction.deleteRole( op, $chip, index ).then(angular.noop, reloadOperator);
        }

        vm.saveWebsiteLogo = function(website){
            WebsiteAction.saveWebsiteLogo(website.id, {'logo': vm.croppedLogo,'name': website.name, 'alias': website.alias, 'description': website.description } ).then(function(){
                vm.croppedLogo = '';

                Toast.show('Website details are saved Successfully')
            });
        }

        //redux done to do implement init function 
        //and place redux code there

        vm.openSetting = function(website, index)
        {
            vm.croppedLogo = '';

            WebsiteAction.loadSetting({'website':website, index:index}).then(function(){
                vm.roles = api.roles.query();
            });
        };

        vm.closeSetting = function()
        {
            return ensureAllFormSaved().then(function(){
                WebsiteAction.closeSetting();
            });
        };

        vm.onPricingOpened = function(type){
            vm.currentSelectedPricingIndex = vm.plans.map(function(p){return p.plan}).indexOf(type);
        }


        function ensureAllFormSaved( )
        {
            if( vm.forms.basicSetting && vm.forms.basicSetting.$dirty !== false )
            {
                vm.saveBasicSettingForm( vm.forms.basicSetting );
            }

            return $q.when(true);
        }

        vm.toogleSettingNav = function() {
            $mdSidenav('setting-sidenav')
                .toggle()
                .then(function() {

                });
        };

        vm.closeDialog = function() {
            $mdDialog.cancel();
        };

        vm.collapseOne = function() {
            $mdExpansionPanel('expansionPanelOne').collapse();
        };

        vm.saveWebsite = function(form) {
            if ( form.$valid ) {
                WebsiteAction.addNewWebsite(vm.new).then(function(web) {

                    vm.openSetting(web, vm.websites.list.map(function(l){ return l.id }).indexOf(web.id));;

                    $mdDialog.hide();
                });
            }
        };

        vm.switchToFree = function(){
            AppSocket.swtichToFreePlan(vm.websites.settingOpened.website.id);
        };

        vm.saveBasicSettingForm = function(form)
        {
            if( form.$valid )
            {
                WebsiteAction.editWebsite( vm.websites.settingOpened );
            }
            else
            {
                WebsiteAction.reverForInvalidForm();
            }
        };

        vm.loadScript = function(){
            WebsiteAction.loadScript().then(function(script){
                vm.websites.settingOpened.website.script = script;
            });
        };

        vm.loadOperators = function()
        {
            WebsiteAction.loadOperators().then(function(operators){

                vm.currentWebsiteOperators = angular.copy(operators);

                vm._currentWebsiteOperators = angular.copy(operators);
            });
        }

        vm.removeUserFromWebsite = function(op)
        {
            WebsiteAction.removeOperator(op);
        }

        vm.saveNewMacro = function(macroForm, $panel){

            if( macroForm.$valid )
            {
                var macro = angular.copy(vm.newMacro);
                
                
                // marco.marcoTypecc

                WebsiteAction.addNewMacro(macro);
                vm.newMacro = {};
                macroForm.$setPristine();
                $panel.collapse();
            }
        };


        vm.loadMacroEditForm = function( macro,index,$panel )
        {
            $panel.collapse();

            ensureMacroSaved().then(function(){
                WebsiteAction.editMacro( { macro: macro, index: index, panel: $panel } );
            });

        };

        vm.removeMarco = function(macro, index){

            WebsiteAction.removeMacro(macro.id, index);

        }

        vm.saveMacroEditForm = function( form )
        {
            if( form && ( form.$dirty === true && form.$valid ) ){
                return WebsiteAction.saveEditingMacro( ).then( function(){
                    vm.websites.editingMacro.panel.collapse();
                    WebsiteAction.closeEditMacroForm();
                });

            } else 
            {
                if( vm.websites.editingMacro &&  vm.websites.editingMacro.panel){
                    vm.websites.editingMacro.panel.collapse();
                    WebsiteAction.closeEditMacroForm();
                }
            }

            return $q.when(true); 
        };

        vm.openMessageTOAllForm = function(ev){

            vm.messageToSend = {'message': '', type: 'push'}

            $mdDialog.show({
                templateUrl: 'app/main/website/dialogs/send-message.dialog.html',
                parent: angular.element(document.body),
                controllerAs: 'vm',
                controller: function(){
                    return vm;
                },
                clickOutsideToClose: true,
                targetEvent: ev,
            });
        }

        vm.sendMessageToAll = function( messageToSend ){
            AppSocket.sendMessageToAll(messageToSend, vm.websites.settingOpened.website.id);
            vm.closeDialog();
        }


        vm.upgradePricing = function(ev, plan){


            vm.selectedPlan = plan;
            vm.selectedPlanPrice = plan.price[0];

            $mdDialog.show({
                templateUrl: 'app/main/website/dialogs/pricing.dialog.html',
                parent: angular.element(document.body),
                controllerAs: 'vm',
                controller: function(){
                    return vm;
                },
                clickOutsideToClose: true,
                targetEvent: ev,
            });

            // api.upgradePlan.get(function(res){
            //     location.href = res.paypalLink; 
            // });

        };

        vm.contactForPricing = function(ev)
        {

        }

        function ensureMacroSaved()
        {
            // if( vm.websites.editingMacro )
            // {
            //     // return vm.saveMacroEditForm( vm.form.macroEditForm );                
            // }

            return $q.when(true);
        }


        function copyToClipboard() {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($('.app-script').text()).select();
            document.execCommand("copy");
            $temp.remove();
            Toast.show('Copied Successfully');
        }


        function clearSelection() {
            if ( document.selection ) {
                document.selection.empty();
            } else if ( window.getSelection ) {
                window.getSelection().removeAllRanges();
            }
        }
    }

    function callSafeDigest(scope, callBack) {
        setTimeout(function () {
            console.log('calling safe')
          scope.$apply(callBack);
        });
    }
})();
