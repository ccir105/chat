(function () {
    'use strict';

    angular.module('app.chat', [])
    .config(config)
    .run(function($rootScope, $mdDialog,$state, Toast){

        $rootScope.$on('support:call_arrived', function(event, data){

            if(angular.element(document).find('md-dialog').length === 0)
            {
                $mdDialog.hide();
            }

             $mdDialog.show(
             {
                templateUrl: 'app/main/apps/chat/dailogs/calling.dialog.html',
                locals:{User: data},
                parent: angular.element(document.body),
                controllerAs: 'vm',
                controller: 'CallingController',
                clickOutsideToClose:false,
            });

        });


        $rootScope.$on('support:request-accepted', function(ev, message){

            $mdDialog.destroy();

            Toast.show(message.adminName + ' has accepted the call');
        }); 

        $rootScope.$on('call-accepted', function(event , data)
        {
            if( $state.current.name !== 'app.chat')
            {
                $state.go('app.chat').then(function(){
                    $rootScope.$broadcast('open-chat',data);
                });
            }
            else
            {
                $rootScope.$broadcast('open-chat',data);
            }
        });

    });
    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {

        // State
        $stateProvider.state('app.chat', {
            url: '/chat',
            views: {
                'content@app': {
                    templateUrl: 'app/main/apps/chat/chat.html',
                    controller: 'ChatController as vm'
                }
            }
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/chat');

        msNavigationServiceProvider.saveItem('apps', {
            title : 'Main Menu',
            group : true,
            weight: 1
        });
        
        // Navigation

        msNavigationServiceProvider.saveItem('apps.chat', {
            title: 'Chat',
            icon: 'icon-hangouts',
            state: 'app.chat',
            // badge: {
            //     content: 13,
            //     color: '#09d261'
            // },
            weight: 5
        });
    }
})();