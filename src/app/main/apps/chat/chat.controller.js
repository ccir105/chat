(function() {
    'use strict';

    angular.module('app.chat').controller('ChatController', ChatController);

    /** @ngInject */
    function ChatController($rootScope, $state, $scope, $mdSidenav, $timeout, $document, $mdMedia, AppSocket, $mdDialog, AppStore, ChatAction) {

        var vm = this;
        var audio = new Audio('assets/audio/audio.mp3');

        var userLoaded;

        var unsubscribe = AppStore.subscribe(function() {
            callSafeDigest($scope, function() {

                if (AppStore.getState().website && AppStore.getState().website.list !== null && AppStore.getState().website.list.length === 0) {
                    $state.go('app.website');
                    return;
                }

                vm.chatState = AppStore.getState().chat;
            });

        });

        vm.askForClientInfo = function(groupName) {
            AppSocket.askForClientInfo(groupName);
        }

        vm.blockVisitor = function(groupName, value) {
            ChatAction.blockVisitor(groupName, value);
        }

        vm.useMacro = function(macro) {
            AppSocket.sendMessage(macro, vm.currentChat.groupName);
            toogleRightSideNavView(false);
            toggleSidenav('right-sidenav');
        }

        vm.loadOlderMessage = function() {
            ChatAction.callLoadMoreMessage();
        }


        var unsubscribeMe = $rootScope.$on('me:loaded', function(ev, me) {
            vm.me = me;
        });

        vm.me = $rootScope.me;

        var openChatFromGlobal = $scope.$on('open-chat', function(event, data) {
            vm.currentChat = data;
        });

        vm.loadUserHistory = function() {
            AppSocket.loadMoreClients(ChatAction.getCurrentPageNo());
        }

        var onMessageReceived = $scope.$on('chat:message-received', function(event, chat) {

            if (chat.notify && chat.message.User.adminGuid !== vm.me.adminGuid) {
                audio.play();
            }

            $timeout(function() {
                scrollToBottomOfChat();
            });

        });

        var unsubscribescrollToBottomOfChat = $scope.$on('chat:scroll-bottom', function() {
            $timeout(function() {
                scrollToBottomOfChat();
            })
        });

        $scope.$on('$destroy', function() {
            unsubscribe();
            unsubscribeMe();
            openChatFromGlobal();
            unsubscribescrollToBottomOfChat();
        });

        ChatAction.init(true);

        vm.getChat = function(chat, index) {

            if (vm.currentChat && vm.currentChat.groupName === chat.groupName) {
                return;
            }

            vm.currentChat = chat;

            if (vm.currentChat.unread) {
                ChatAction.clearUndreadOnChat(chat.groupName);
            }

            ChatAction.setActiveGroupName(chat.groupName);

            $timeout(function() {
                scrollToBottomOfChat();
            });

            $mdSidenav('left-sidenav').close();
        };

        function reply($event) {
            if ($event && $event.keyCode === 13 && $event.shiftKey) {
                vm.textareaGrow = true;
                return;
            }

            if ($event && $event.keyCode !== 13) {

                onKeyDownNotEnter();

                return;
            }

            if (vm.replyMessage === '') {
                resetReplyTextarea();
                return;
            }

            AppSocket.sendMessage(vm.replyMessage, vm.currentChat.groupName);

            resetReplyTextarea();
        }

        vm.leftSidenavView = false;
        vm.rightSidenavView = false;

        vm.chat = undefined;
        // Methods

        vm.toggleSidenav = toggleSidenav;

        vm.toggleLeftSidenavView = toggleLeftSidenavView;

        vm.toogleRightSideNavView = toogleRightSideNavView;

        vm.reply = reply;

        vm.setUserStatus = setUserStatus;

        vm.clearMessages = clearMessages;

        var populateNewUser = $scope.$on('app:new-user-authorized', function(ev, data) {
            callSafeDigest($scope, function() {
                vm.contacts.push(data);
            });
        });


        vm.checkIfInfoDb = function() {
            AppSocket.loadIpInfoDb(vm.currentChat.groupName);
        }

        var userLeft = $scope.$on('app:user-offline', function(ev, data) {
            callSafeDigest($scope, function() {
                vm.contacts.map(function(contact, key) {
                    if (contact.connectionId === data.connectionId) {
                        vm.contacts.splice(key, 1);
                        return;
                    }
                    return;
                });
            });
        });

        // var onMessageReceived = $scope.$on('app:message-received', function(ev, message){

        //     // Add the message to the chat

        //      if( !angular.isUndefined(vm.chat) ){
        //         vm.chat.push(message);
        //         scrollToBottomOfChat();
        //      }

        //     // Reset the reply textarea

        //     // Scroll to the new message
        // });

        var onUserCalling = $scope.$on('app:user-calling', function(event, connectionId) {

            vm.contacts.map(function(user) {
                if (user.connectionId === connectionId) {
                    vm.currentChat = user;
                }
            });

            callSafeDigest($scope, function() {
                ringing();
            });
        });

        var afterCallAccepted = $scope.$on('app:after-call-accepted', function(event, message) {
            callSafeDigest($scope, function() {
                vm.chat = vm.chat.concat(message);
                scrollToBottomOfChat();
            });
        });

        var onSupporRequestAccepted = $scope.$on('app:support-request-accepted', function(event, user) {
            callSafeDigest($scope, function() {
                vm.supportBoxMessage = 'Somethin has accepted';
            });
        });

        var onUserTyping = $scope.$on('app:user-typing', function(event, user) {
            callSafeDigest($scope, function() {
                vm.userTyping = user.f;
            });
        });

        var onUserStoppedTyping = $scope.$on('app:user-stop-typing', function(event, user) {
            callSafeDigest($scope, function() {
                vm.userTyping = null;
            });
        });

        //////////

        /**
         * Get Chat by Contact id
         * @param contactId
         */

        function ringing() {
            $mdDialog.show({
                templateUrl: 'app/main/apps/chat/dailogs/calling.dialog.html',
                parent: angular.element(document.body),
                controllerAs: 'vm',
                controller: function() {
                    return vm;
                },
                clickOutsideToClose: false,
                targetEvent: event,
            });
        }

        vm.onCallAccepted = function() {
            $mdDialog.hide();
            vm.chat = [];
            AppSocket.callAccepted(vm.currentChat.connectionId);
        };

        vm.onCallDeclined = function() {
            vm.supportBoxMessage = undefined;
            $mdDialog.cancel();
        };



        vm.testPagination = function() {
            console.log('Teting')
        }

        /**
         * Reply
         */


        /**
         * Clear Chat Messages
         */
        function clearMessages() {
            vm.chats[vm.chatContactId] = vm.chat = [];
            vm.contacts.getById(vm.chatContactId).lastMessage = null;
        }

        /**
         * Reset reply textarea
         */
        function resetReplyTextarea() {
            vm.replyMessage = '';
            vm.textareaGrow = false;
        }

        /**
         * Scroll Chat Content to the bottom
         * @param speed
         */
        function scrollToBottomOfChat() {
            $timeout(function() {
                var chatContent = angular.element($document.find('.chat .md-virtual-repeat-scroller'));
                if (chatContent.length !== 0) {
                    chatContent.animate({
                        scrollTop: chatContent[0].scrollHeight
                    }, 400);
                }
            }, 0);
        }

        /**
         * Set User Status
         */
        function setUserStatus(status) {
            vm.user.status = status;
        }

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId) {
            $mdSidenav(sidenavId).toggle();
        }

        vm.loadMacroList = function() {
            ChatAction.loadCurrentMacros(vm.currentChat.webSite).then(function(res) {
                vm.toogleRightSideNavView('macro-list');
                vm.macroList = res;
                vm.toggleSidenav('right-sidenav');
            });
        }

        /**
         * Toggle Left Sidenav View
         *
         * @param view id
         */
        function toggleLeftSidenavView(id) {
            vm.leftSidenavView = id;
        }

        function toogleRightSideNavView(id) {
            vm.rightSidenavView = id;
        }


        /*********************/

        var typing = false;

        var timeout = null;

        function onKeyDownNotEnter() {
            if (typing === false) {
                typing = true;

                AppSocket.userTyping(vm.currentChat.groupName);

                timeout = $timeout(onTypingFinished, 3000);
            } else {
                $timeout.cancel(timeout);

                timeout = $timeout(onTypingFinished, 3000);

            }
        }

        function onTypingFinished() {
            typing = false;

            AppSocket.stoppedTyping(vm.currentChat.groupName);
        }

        angular.element(document).find('#chat-input').on('blur', function() {
            typing = false;
        });

    }

    function callSafeDigest(scope, callBack) {
        setTimeout(function() {
            console.log('calling safe')
            scope.$apply(callBack);
        });
    }

})();