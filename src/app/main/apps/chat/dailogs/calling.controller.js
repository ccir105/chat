(function ()
{
    'use strict';

    angular
        .module('app.chat')
        .controller('CallingController', Controller);

        function Controller($rootScope, $scope, $mdDialog, User, ChatAction)
        {
        	var vm = this;

        	vm.onCallAccepted = function(){
        		ChatAction.acceptCall( User ).then(function(user){
        			$mdDialog.hide( );
        			ChatAction.setActiveChatUser( user );
        			$rootScope.$broadcast('call-accepted', user);
        		});
        	};

        	vm.onCallDeclined = function(){
        		$mdDialog.hide();
        	};
        }
})();