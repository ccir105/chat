(function ()
{
    'use strict';

    angular
        .module('app.dashboards.analytics')
        .controller('DashboardAnalyticsController', DashboardAnalyticsController);

    /** @ngInject */
    function DashboardAnalyticsController(DashboardData, $scope, AppStore, uiGmapGoogleMapApi, api, WebsiteAction, AppSocket)
    {
        var vm = this;

        vm.websites = WebsiteAction.getState().list;

        var unsubscribeSinglarConnected = $scope.$on('singlar:connected', function(){

            WebsiteAction.loadWebsites();

        });

        var unSubscribeDashBoardData  = $scope.$on('socket:refresh-analytics', function(ev, data){
            safeDigest($scope, function(){
               initDashboardData(data);
            });
        });


        var unsubscribeRedux = AppStore.subscribe(function(){

            vm.websites = AppStore.getState().website.list;

            if( vm.lastSelectedWebsiteIndex === undefined){
                initWebsiteSelection();
            }

        });


        if( $scope.singlarConnected && AppStore.getState().website &&  AppStore.getState().website.list )
        {
            vm.websites = AppStore.getState().website.list;

            initWebsiteSelection();
        }

        $scope.$on('$destroy', function ()
        {
            unsubscribeSinglarConnected();
            unsubscribeRedux();
            unSubscribeDashBoardData();
        });


        vm.initWebsiteSelection = initWebsiteSelection

        function initWebsiteSelection(index){
             
            index = index === undefined ? 0 : index;

            if( vm.websites && vm.websites[ index ])
            {
                var website = vm.websites[ index ];

                vm.selectedWebsite = website;

                vm.lastSelectedWebsiteIndex = index;

                AppSocket.getAnalyticsReport(website.id);
            }

        }

        // Data
        vm.dashboardData = DashboardData;
        vm.colors = ['blue-bg', 'blue-grey-bg', 'orange-bg', 'pink-bg', 'purple-bg'];

        function initWidget1(response)
        {
            console.log(response, DashboardData.widget1);

            vm.widget1 = {
            title             : response.title,
            onlineUsers       : response.onlineUsers,
            bigChart          : {
                options: {
                    chart: {
                        type                   : 'lineWithFocusChart',
                        color                  : ['#2196F3'],
                        height                 : 400,
                        margin                 : {
                            top   : 32,
                            right : 32,
                            bottom: 64,
                            left  : 48
                        },
                        isArea                 : true,
                        useInteractiveGuideline: true,
                        duration               : 1,
                        clipEdge               : true,
                        clipVoronoi            : false,
                        interpolate            : 'cardinal',
                        showLegend             : false,
                        x                      : function (d)
                        {
                            return d.x;
                        },
                        y                      : function (d)
                        {
                            return d.y;
                        },
                        xAxis                  : {
                            showMaxMin: false,
                            tickFormat: function (d)
                            {
                                var date = new Date(new Date().setDate(new Date().getDate() + d));
                                return d3.time.format('%b %d')(date);
                            }
                        },
                        yAxis                  : {
                            showMaxMin: false
                        },
                        x2Axis                 : {
                            showMaxMin: false,
                            tickFormat: function (d)
                            {
                                var date = new Date(new Date().setDate(new Date().getDate() + d));
                                return d3.time.format('%b %d')(date);
                            }
                        },
                        y2Axis                 : {
                            showMaxMin: false
                        },
                        interactiveLayer       : {
                            tooltip: {
                                gravity: 's',
                                classes: 'gravity-s'
                            }
                        },
                        legend                 : {
                            margin    : {
                                top   : 8,
                                right : 0,
                                bottom: 32,
                                left  : 0
                            },
                            rightAlign: false
                        }
                    }
                },
                data   : response.bigchart.chart
            },
            sessions          : {
                title   : response.sessions.title,
                value   : response.sessions.value,
                previous: response.sessions.previous,
                options : {
                    chart: {
                        type                   : 'lineChart',
                        color                  : ['#03A9F4'],
                        height                 : 40,
                        margin                 : {
                            top   : 4,
                            right : 4,
                            bottom: 4,
                            left  : 4
                        },
                        isArea                 : true,
                        interpolate            : 'cardinal',
                        clipEdge               : true,
                        duration               : 500,
                        showXAxis              : false,
                        showYAxis              : false,
                        showLegend             : false,
                        useInteractiveGuideline: true,
                        x                      : function (d)
                        {
                            return d.x;
                        },
                        y                      : function (d)
                        {
                            return d.y;
                        },
                        xAxis                  : {
                            tickFormat: function (d)
                            {
                                var date = new Date(new Date().setDate(new Date().getDate() + d));
                                return d3.time.format('%A, %B %d, %Y')(date);
                            }
                        },
                        interactiveLayer       : {
                            tooltip: {
                                gravity: 's',
                                classes: 'gravity-s'
                            }
                        }
                    }
                },
                data    : response.sessions.chart
            },
            pageviews         : {
                title   : response.pageviews.title,
                value   : response.pageviews.value,
                previous: response.pageviews.previous,
                options : {
                    chart: {
                        type                   : 'lineChart',
                        color                  : ['#3F51B5'],
                        height                 : 40,
                        margin                 : {
                            top   : 4,
                            right : 4,
                            bottom: 4,
                            left  : 4
                        },
                        isArea                 : true,
                        interpolate            : 'cardinal',
                        clipEdge               : true,
                        duration               : 500,
                        showXAxis              : false,
                        showYAxis              : false,
                        showLegend             : false,
                        useInteractiveGuideline: true,
                        x                      : function (d)
                        {
                            return d.x;
                        },
                        y                      : function (d)
                        {
                            return d.y;
                        },
                        xAxis                  : {
                            tickFormat: function (d)
                            {
                                var date = new Date(new Date().setDate(new Date().getDate() + d));
                                return d3.time.format('%A, %B %d, %Y')(date);
                            }
                        },
                        interactiveLayer       : {
                            tooltip: {
                                gravity: 's',
                                classes: 'gravity-s'
                            }
                        }
                    }
                },
                data    : response.pageviews.chart
            },
            pagesSessions     : {
                title   : response.pagesSessions.title,
                value   : response.pagesSessions.value,
                previous: response.pagesSessions.previous,
                options : {
                    chart: {
                        type                   : 'lineChart',
                        color                  : ['#E91E63'],
                        height                 : 40,
                        margin                 : {
                            top   : 4,
                            right : 4,
                            bottom: 4,
                            left  : 4
                        },
                        isArea                 : true,
                        interpolate            : 'cardinal',
                        clipEdge               : true,
                        duration               : 500,
                        showXAxis              : false,
                        showYAxis              : false,
                        showLegend             : false,
                        useInteractiveGuideline: true,
                        x                      : function (d)
                        {
                            return d.x;
                        },
                        y                      : function (d)
                        {
                            return d.y;
                        },
                        xAxis                  : {
                            tickFormat: function (d)
                            {
                                var date = new Date(new Date().setDate(new Date().getDate() + d));
                                return d3.time.format('%A, %B %d, %Y')(date);
                            }
                        },
                        interactiveLayer       : {
                            tooltip: {
                                gravity: 's',
                                classes: 'gravity-s'
                            }
                        }
                    }
                },
                data    : response.pagesSessions.chart
            },
            avgSessionDuration: {
                title   : response.avgSessionDuration.title,
                value   : response.avgSessionDuration.value,
                previous: response.avgSessionDuration.previous,
                options : {
                    chart: {
                        type                   : 'lineChart',
                        color                  : ['#009688'],
                        height                 : 40,
                        margin                 : {
                            top   : 4,
                            right : 4,
                            bottom: 4,
                            left  : 4
                        },
                        isArea                 : true,
                        interpolate            : 'cardinal',
                        clipEdge               : true,
                        duration               : 500,
                        showXAxis              : false,
                        showYAxis              : false,
                        showLegend             : false,
                        useInteractiveGuideline: true,
                        x                      : function (d)
                        {
                            return d.x;
                        },
                        y                      : function (d)
                        {
                            return d.y;
                        },
                        xAxis                  : {
                            tickFormat: function (d)
                            {
                                var date = new Date(new Date().setDate(new Date().getDate() + d));
                                return d3.time.format('%A, %B %d, %Y')(date);
                            }
                        },
                        yAxis                  : {
                            tickFormat: function (d)
                            {
                                var formatTime = d3.time.format('%M:%S');
                                return formatTime(new Date('2012', '0', '1', '0', '0', d));
                            }
                        },
                        interactiveLayer       : {
                            tooltip: {
                                gravity: 's',
                                classes: 'gravity-s'
                            }
                        }
                    }
                },
                data    : response.avgSessionDuration.chart
            }
        };
        }

     

        function initDashboardData(response){

            if(  response.widget1 )
            {
                initWidget1(response.widget1);
            }

            if( response.widget2 )
            {
                vm.widget2 = {
                    title: response.widget2.title
                };

                uiGmapGoogleMapApi.then(function ()
                {
                    vm.widget2.map = response.widget2.map;
                });
            }

            if( response.widget3 )
            {
                console.log(response.widget3, DashboardData.widget3)
                vm.widget3 = {
                    title       : response.widget3.title,
                    pages       : response.widget3.pages,
                    ranges      : response.widget3.ranges,
                    currentRange: response.widget3.currentRange,
                    changeRange : function (range)
                    {
                        vm.widget3.currentRange(range);
                    }
                };
            }

            if( response.widget4 )
            {
                vm.widget4 = response.widget4;
            }
        }

      
        // Widget 4


        // Methods

        //////////

        // Widget 2
      
    }


    function safeDigest($scope, callBack){
        setTimeout(function(){
            $scope.$apply(callBack);
        });
    }

})();