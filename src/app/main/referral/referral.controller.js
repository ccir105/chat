(function ()
{
    'use strict';

    angular
        .module('app.referral')
        .controller('ReferralController', ctrl );

        function ctrl($scope, AppStore, ChatAction, $mdDialog, $timeout )
        {
        	var vm = this;

        	vm.openAddReferral = openAddReferral

            var unsubscribe = AppStore.subscribe(function(){
                callSafeDigest($scope, function(){

                    if( vm.referrals !== null )
                    {
                        vm.referrals = AppStore.getState().chat.referral || [];
                    }

                });
            });

            ChatAction.loadReferrals();

        	function openAddReferral(ev){

        		vm.current = {'fullName': '','email': ''}

        		$mdDialog.show({
	                templateUrl: 'app/main/referral/dialogs/referal.dialog.html',
	                parent: angular.element(document.body),
	                controllerAs: 'vm',
	                controller: function() {
	                    return vm;
	                },
	                clickOutsideToClose: true,
	                targetEvent: ev,
	            })
        	}

        	vm.closeDialog = function(){
        		$mdDialog.cancel();
        	}

        	vm.saveReferral = function(){
                ChatAction.saveReferral(vm.current).then(vm.closeDialog, vm.closeDialog);
        	}
        }

        function callSafeDigest(scope, callBack) {
            setTimeout(function () {
                console.log('calling safe')
              scope.$apply(callBack);
            });
        }
})();