(function ()
{
    'use strict';

    angular
        .module('app.referral',[])
        .config(config);

      /** @ngInject */
	    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {

	        // State
	        $stateProvider.state('app.referral', {
	            url: '/referrals',
	            views: {
	                'content@app': {
	                    templateUrl: 'app/main/referral/referral.html',
	                    controller: 'ReferralController as vm'
	                }
	            }
	        });

	        msNavigationServiceProvider.saveItem('apps.referral', {
	            title: 'Referral',
	            icon: 'icon-account-network',
	            state: 'app.referral',
	            weight: 5
	        });
	    }
})();