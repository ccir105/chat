(function ()
{
    'use strict';

    angular
        .module('fuse')
        .run(runBlock);

    /** @ngInject */
    function runBlock($rootScope, $timeout, $state, api, $window, ChatAction, $mdToast, AppSocket, $mdMedia)
    {
        $rootScope.$mdMedia = $mdMedia;

        ChatAction.init();

        
        if( "Notification" in window )
        {
            Notification.requestPermission().then(function(result) {
              $rootScope.notificationEnabled = result;
            });
        }

        $window.onfocus = function(){
            ChatAction.windowFocused();
        };

        $window.onblur = function(){
           ChatAction.windowBlurred();
        };

        $rootScope.isMobile = function(){
            return typeof window.orientation !== 'undefined';
        };


        $rootScope.closeErrorToast = function(){
            $mdToast.hide()
        }

        $rootScope.isArray = function(value){
            return angular.isArray(value);
        }


        // Activate loading indicator
        var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function ()
        {
            $rootScope.loadingProgress = true;
        });

        api.profile.get(function(res){
            $rootScope.me = res;
            $rootScope.$broadcast('me:loaded',res);
        }); 

        $rootScope.logout = function(){
            if($rootScope.me.logoffUrl){
                location.href = $rootScope.me.logoffUrl;
            }
        };

        // De-activate loading indicator
        var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function ()
        {
            $timeout(function ()
            {
                $rootScope.loadingProgress = false;
            });
        });

        // Store state in the root scope for easy access
        $rootScope.state = $state;

        var errorUnscribe = $rootScope.$on('app:error', function(ev, response){
                
            if( response.data && response.data.message )
            {
                showCustomToast(response.data.message);            
            }
        });  

        $rootScope.$broadcast('socket:new-toast', function(ev, message){
            showCustomToast(message);
        });


        function showCustomToast(res){

            $rootScope.lastErrorMessage = res;

            $mdToast.show({
              hideDelay   : res.delay === 0 ? 0 : 5000, 
              position    : 'top right',
              templateUrl : 'app/main/dialogs/error-toast.html'
            });
        }


        $rootScope.$on('no:internet', function(){
            showCustomToast('No Internet Available');
        });

        $rootScope.$on('$destroy', function ()
        {
            stateChangeStartEvent();
            stateChangeSuccessEvent();
            errorUnscribe();
        });
    }
})();