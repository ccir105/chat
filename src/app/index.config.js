(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('interceptor', interceptor)
        .constant('BASE_URL', baseName)
        .config(config);

    /** @ngInject */
    function config($translateProvider,$httpProvider) {
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '{part}/i18n/{lang}.json'
        });

        $translateProvider.preferredLanguage('en');
        $httpProvider.interceptors.push('interceptor');

        $translateProvider.useSanitizeValueStrategy('sanitize');
    }

    function interceptor($rootScope, $q) {
        return {
            request: function(config) {

                $rootScope.loadingProgress = true;

                config.headers = config.headers || {};

                return $q.when(config);
            },

            response: function(response)
            {
                $rootScope.loadingProgress = false;
                return response;
            },

            responseError: function(res) {

                if (res.status === -1 || res.status === 0) {
                    $rootScope.$broadcast('error:no_internet', res);
                }

                $rootScope.loadingProgress = false;

                $rootScope.$broadcast('app:error',res);

                return $q.reject(res);
            }
        };
    }



})();
