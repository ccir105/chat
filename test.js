angular.module('demo', []);

angular.module('demo').service('Redux', function (){
  return Redux;
});

angular.module('demo').service('listReducer', function () {
  return function(state = {list: Immutable.List([])}, action) {
    switch (action.type) {
      case 'ADD_ITEM':
        state.list = state.list.push(Immutable.Map({value: '', added: false}));
        return state;
      case 'REMOVE_ITEM':
        state.list = state.list.splice(action.index, 1);
        return state;
      case 'CONFIRM_ITEM':
        state.list = state.list.setIn([action.index, 'added'] ,true)
        return state;
      case 'MODIFY_ITEM':
        state.list = state.list.setIn([action.index, 'value'], action.value)
        return state
      default:
        return state;
    }
  }
});

angular.module('demo').service('applicationStore', ['Redux', 'listReducer', function (Redux, listReducer) {
  var initialState = {
    list: Immutable.List([])
  };
  return Redux.createStore(listReducer, initialState);
}]);

angular.module('demo').directive('listDirective', function (applicationStore) {
  return {
    templateUrl: 'list.html',
    restrict: 'E',
    link: function(scope) {
      applicationStore.subscribe(function () {
        scope.list = applicationStore.getState().list.toJS();
      })
      
      scope.addItem = function() {
        applicationStore.dispatch({
          type: 'ADD_ITEM'
        });
      } 
    }
  }
})

angular.module('demo').directive('listItem', function (applicationStore) {
  return {
    templateUrl: 'item.html',
    restrict: 'E',
    scope: {
      item: '=',
      removalIndex: '@'
    },
    link: function(scope) {
      scope.remove = function() {
        applicationStore.dispatch({
          type: 'REMOVE_ITEM',
          index: scope.removalIndex
        });
      }
      
      scope.confirmItem = function() {
        applicationStore.dispatch({
          type: 'CONFIRM_ITEM',
          index: scope.removalIndex
        });
      }
      
      scope.change = function(localValue) {
        applicationStore.dispatch({
          type: 'MODIFY_ITEM',
          index: scope.removalIndex,
          value: localValue
        })
      }
    }
  }
})